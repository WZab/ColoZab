Nasze repozytorium do dzielenia się kodami tworzonymi dla gry Colobot.
W podkatalogu src można umieszczać kody tworzonych procedur.
W przypadku tworzenia bardziej złożonych projektów, proszę tam zakładać podkatalogi.
W razie większych zmian, proszę tworzyć rozgałęzienia (branche), które później będziemy scalać.